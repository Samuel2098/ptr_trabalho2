#include "./biblioteca/structs.h"
#include "./biblioteca/estatisticas.h"
#include "monitor.h"
#include "./blocos/entrada.h"
#include "./blocos/modelo.h"
#include "./blocos/controle.h"
#include "./blocos/linearizador.h"
#include "./blocos/robo.h"

/* Declaração Global dos semáforos */
sem_t produzEntrada;
sem_t produzReferenciador,consomeReferenciador;
sem_t consomeControladorReferenciador,consomeControladorRobot,produzControlador;
sem_t produzRobotLinearizador,produzRobotControlador,consomeRobot;
sem_t consomeLinearizadorControlador,consomeLinearizadorRobot,produzLinearizador;

/* Declaração global das matrizes */
Matrix *YM=(Matrix*)malloc(sizeof(Matrix));
Matrix *YMD=(Matrix*)malloc(sizeof(Matrix));
Matrix *REF=(Matrix*)malloc(sizeof(Matrix));
Matrix *V=(Matrix*)malloc(sizeof(Matrix));
Matrix *Y=(Matrix*)malloc(sizeof(Matrix));
Matrix *X=(Matrix*)malloc(sizeof(Matrix));
Matrix *XD=(Matrix*)malloc(sizeof(Matrix));
Matrix *U=(Matrix*)malloc(sizeof(Matrix));

/* Declaração global da struct dos argumentos */
Arguments Args;

/* Declaração global das variáveis de estatísticas */
// //Período
double periodo_tarefa_1[TAMANHO];
double periodo_tarefa_2[TAMANHO];
double periodo_tarefa_3[TAMANHO];
double periodo_tarefa_4[TAMANHO];
double periodo_tarefa_5[TAMANHO];
// //Jitter
double jitter_tarefa_1[TAMANHO];
double jitter_tarefa_2[TAMANHO];
double jitter_tarefa_3[TAMANHO];
double jitter_tarefa_4[TAMANHO];
double jitter_tarefa_5[TAMANHO];

//Periodo
double period1[TAMANHO];
double period2[TAMANHO];
double period3[TAMANHO];
double period4[TAMANHO];
double period5[TAMANHO];


/* Bloco do robô */
void* Robot(void *arg){
    clock_t start, end; 
    double intervalo;
    while(Args.tempo<=20){
        start = clock();
        sem_wait(&consomeRobot);
            // consome u(t)
            robo_consome_u(Args, U, X, XD);
        sem_post(&produzLinearizador);

        sem_wait(&produzRobotLinearizador);
            // produz x(t)
            robo_produz_x(X);
        sem_post(&consomeLinearizadorRobot);

        sem_wait(&produzRobotControlador);
            // produz y(t)
            robo_produz_y(Y, X);
        sem_post(&consomeControladorRobot);
        end = clock(); 
        intervalo=difftime(end,start);
        period5[Args.posicao2]=30-intervalo/1000;
        jitter_tarefa_1[Args.posicao5] = (0.03*1000 - intervalo/1000);
        Args.posicao5++;
        usleep(30000 - intervalo);
    }
}

/* Bloco  Linearização */
void* Linearizador(void *arg){
    clock_t start, end; 
    double intervalo;
    while(Args.tempo<=20){
        start = clock();
        sem_wait(&consomeLinearizadorControlador);
                // consome v(t)
                linearizador_consome_v(V);
        sem_post(&produzControlador);

        sem_wait(&consomeLinearizadorRobot);
            // consome x(t)
            // linearizador_consome_x(X);
        sem_post(&produzRobotLinearizador);

        sem_wait(&produzLinearizador);
            // produz u(t)
            linearizador_produz_u(U,X,V);
        sem_post(&consomeRobot);
        // usleep(40000);
        end = clock(); 
        intervalo=difftime(end,start);
        period4[Args.posicao2]=40-intervalo/1000;
        jitter_tarefa_2[Args.posicao4] = (0.04*1000 - intervalo/1000);
        Args.posicao4++;
        usleep( 40000 - intervalo);
    }
}

/* Bloco Controle */
void* Controlador(void *arg){
    clock_t start, end; 
    double intervalo=0;

    while(Args.tempo<=20){
        start = clock();
        sem_wait(&consomeControladorReferenciador);
            //Consome [Ymy,Ymx] e [Ymy',Ymx']
            controle_consome_ym();
        sem_post(&produzReferenciador);

        sem_wait(&consomeControladorRobot);
            //Consome [y1,y2]
            // controle_consome_y(Y);
        sem_post(&produzRobotControlador);

        sem_wait(&produzControlador);
            // produz v(t)
            controle_produz_v(V, Y,YM, YMD);
        sem_post(&consomeLinearizadorControlador);
        // usleep(50000);
        end = clock(); 
        intervalo=difftime(end,start);
        period3[Args.posicao2]=50-intervalo/1000;
        jitter_tarefa_3[Args.posicao3] = (0.05*1000 - intervalo/1000);
        Args.posicao3++;
        usleep( 50000 -intervalo);
    }
}

/* Bloco Modelo Referência */
void* Referenciador(void *arg){
    clock_t start, end; 
    double intervalo=0;

    while(Args.tempo<=20){
        start = clock();
        sem_wait(&consomeReferenciador);
            
        sem_post(&produzEntrada);
   
        sem_wait(&produzReferenciador);
            // Consome [Xref,Yref]
            modelo_produz_ym(REF, YM, YMD,Args);
        sem_post(&consomeControladorReferenciador);
        // usleep(50000);
        end = clock(); 
        intervalo=difftime(end,start);
        period2[Args.posicao2]=50-intervalo/1000;
        jitter_tarefa_4[Args.posicao2] = (0.05*1000 -intervalo/1000);
        Args.posicao2++;
        usleep( 50000 - intervalo);
    }
}

/* Bloco de entrada */
void* Entrada(void *arg){
    clock_t start, end,end2;  
    double intervalo=0;
    while(Args.tempo<=20){
        start = clock();
        sem_wait(&produzEntrada);
            entrada_produz(&Args,REF);
        sem_post(&consomeReferenciador);
        end = clock(); 

        intervalo=difftime(end,start);

        period1[Args.posicao1]=120-intervalo/1000;

        Args.tempoPassado=Args.tempo;
        Args.tempo+=0.12;

        jitter_tarefa_5[Args.posicao1] = (0.12*1000 - intervalo/1000);
        Args.posicao1+=1;
        end2=end;
        usleep( 120000 - intervalo );
    } 
    sem_post(&consomeReferenciador);
}

void setup(){
    /* Inicialização do tempo */
    Args.tempo = 0;
    Args.tempoPassado=0;
    Args.posicao1=0;
    Args.posicao2=0;
    Args.posicao3=0;
    Args.posicao4=0;
    Args.posicao5=0;

    /* Inicialização da Matriz de entrada u(t)*/
    *U=matrix_zeros("U",2,1);
    *X= matrix_zeros("X",3,1);
    *XD= matrix_zeros("X",3,1);
    *REF=matrix_zeros("REF",2,1);
    *Y=matrix_zeros("Y",2,1);
    *V=matrix_zeros("V",2,1);
    *YM=matrix_zeros("YM",2,1);
    *YMD=matrix_zeros("YMD",2,1);

}
void run(){
    setup();
    /* Declaração das threads */
    pthread_t tarefa_1,tarefa_2,tarefa_3,tarefa_4,tarefa_5;

 
    /* Inicialização dos semáforos */

    //Travas da thread Entrada
    sem_init(&produzEntrada,0,1);
    sem_init(&consomeReferenciador,0,0);

    //Travas da thread Referenciador
    sem_init(&produzReferenciador,0,1);
    sem_init(&consomeControladorReferenciador,0,0);

    //Travas da thread Controlador
    sem_init(&consomeControladorRobot,0,1);
    sem_init(&produzControlador,0,1);


    //travas da thead Linearizador
    sem_init(&consomeLinearizadorControlador,0,0);
    sem_init(&consomeLinearizadorRobot,0,1);
    sem_init(&produzLinearizador,0,1);

    //TRavas da thread Robot
    sem_init(&consomeRobot,0,0);
    sem_init(&produzRobotLinearizador,0,1);
    sem_init(&produzRobotControlador,0,1);

    /* Criação das threads */
    pthread_create(&tarefa_1,NULL,Entrada,NULL);
    pthread_create(&tarefa_2,NULL,Referenciador,NULL);
    pthread_create(&tarefa_3,NULL,Controlador,NULL);
    pthread_create(&tarefa_4,NULL,Linearizador,NULL);
    pthread_create(&tarefa_5,NULL,Robot,NULL);

    /* Inscrição das threads */
    pthread_join(tarefa_1,NULL);
    pthread_join(tarefa_2,NULL);
    pthread_join(tarefa_3,NULL);
    pthread_join(tarefa_4,NULL);
    pthread_join(tarefa_5,NULL);

    estatisticas(jitter_tarefa_1, Args.posicao1);
    estatisticas(jitter_tarefa_2, Args.posicao2);
    estatisticas(jitter_tarefa_3, Args.posicao3);
    estatisticas(jitter_tarefa_4, Args.posicao4);
    estatisticas(jitter_tarefa_5, Args.posicao5);
    
    vetor(period1,Args.posicao1);
    vetor(period2,Args.posicao1);
    vetor(period3,Args.posicao1);
    vetor(period4,Args.posicao1);
    vetor(period5,Args.posicao1);
    return;
}