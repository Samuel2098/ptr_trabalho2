#ifndef __MATRIX_H
#define __MATRIX_H

#include "string.h"

#define M_MAX 10
#define N_MAX 10

#define MAX_NAME_SIZE 20
#define MAX_FUNCTION_SIZE 20

typedef struct
{
    char name[MAX_NAME_SIZE];
    int linhas;
    int colunas;
    double values[N_MAX][M_MAX];
} Matrix;

typedef struct
{
    char name[MAX_NAME_SIZE];
    int linhas;
    int colunas;
    char values[N_MAX][M_MAX][MAX_FUNCTION_SIZE];
} Matrix_str;

//Matrix_str
void matrix_str_print(Matrix_str matriz);
Matrix_str matrix_str_create(char nome[MAX_NAME_SIZE], int linhas, int colunas);

//Matrix
void matrix_print(Matrix matriz);
double matrix_determinant(Matrix matriz);
Matrix matrix_create(char nome[MAX_NAME_SIZE], int linhas, int colunas, int dado);
Matrix matrix_zeros(char nome[MAX_NAME_SIZE], int linhas, int colunas);
Matrix matrix_ones(char nome[MAX_NAME_SIZE], int linhas, int colunas);
Matrix matrix_identity(char nome[MAX_NAME_SIZE], int linhas, int colunas);
Matrix matrix_sum(Matrix matriz_1, Matrix matriz_2);
Matrix matrix_subtraction(Matrix matriz_1, Matrix matriz_2);
Matrix matrix_multiplication(Matrix matriz_1, Matrix matriz_2);
Matrix matrix_scalar(Matrix matriz, double escalar);
Matrix matrix_transposed(Matrix matriz);
Matrix matrix_submatrix(Matrix matrix, int line, int column);
Matrix cofactor(Matrix matriz);
Matrix matrix_inverse(Matrix matriz);
double matrix_det_ordem2(Matrix matriz);

#endif