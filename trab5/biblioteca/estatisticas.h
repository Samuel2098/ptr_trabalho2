#ifndef __ESTATISTICAS_H
#define __ESTATISTICAS_H
#include "structs.h"

double media(double vector[]);
double variancia(double vector[]); 
double desvio_padrao(double vector[]);
double maximo(double vector[]);
double minimo(double vector[]);
void estatisticas(double vector[], int posicao);
void vetor(double vector[],int tamanho);
#endif