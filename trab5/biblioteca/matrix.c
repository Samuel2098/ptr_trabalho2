#include "matrix.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/* 

    Matrix_str

*/

Matrix_str matrix_str_create(char nome[MAX_NAME_SIZE], int linhas, int colunas){
    Matrix_str matrix;
    strcpy(matrix.name, nome);
    matrix.linhas = linhas;
    matrix.colunas = colunas;
    for (int i=0; i<linhas; i++){
        for(int j=0; j<colunas; j++){
            //matrix.values[i][j] = (char*)malloc(MAX_FUNCTION_SIZE*sizeof(char));
            //matrix.values[i][j] = "";
            strcpy(matrix.values[i][j], "");
        }
    }
    return matrix;
}

void matrix_str_print(Matrix_str matriz){
    if ((matriz.colunas < 0 || matriz.colunas >= M_MAX) || (matriz.linhas < 0 || matriz.linhas >= N_MAX)){
        printf("ErrO: LIMITES DA MATRIX ESTOURADOS");
        exit(-1);
    }

    printf("%s (%dx%d)\n", matriz.name, matriz.linhas, matriz.colunas);

    for (int i=0; i<matriz.linhas; i++){
        printf("\t| ");
        for(int j=0; j<matriz.colunas; j++){
            printf("%s ",matriz.values[i][j]);
        }
        printf("|\n");
    }
    printf("\n");
}


/* 

    Matrix 

*/

Matrix matrix_create(char nome[MAX_NAME_SIZE], int linhas, int colunas,int dado){
    Matrix matriz;
    strcpy(matriz.name, nome);
    matriz.linhas = linhas;
    matriz.colunas = colunas;
    for (int i=0; i<linhas; i++){
        for(int j=0; j<colunas; j++){
            matriz.values[i][j] = dado;
        }
    }
    return matriz;
}

Matrix matrix_ones(char nome[MAX_NAME_SIZE], int linhas, int colunas){
    return matrix_create(nome,linhas,colunas,1);
}

Matrix matrix_zeros(char nome[MAX_NAME_SIZE], int linhas, int colunas){
    return matrix_create(nome,linhas,colunas,0);
}

Matrix matrix_identity(char nome[MAX_NAME_SIZE], int linhas, int colunas){
    Matrix matriz;
    strcpy(matriz.name, nome);
    matriz.linhas = linhas;
    matriz.colunas = colunas;
    for (int i=0; i<linhas; i++){
        for(int j=0; j<colunas; j++){
            if(i==j){
                matriz.values[i][j] = 1;
            }
            else{
                matriz.values[i][j] = 0;
            }
        }
    }

    return matriz;
}

void matrix_print(Matrix matriz){
    if ((matriz.colunas < 0 || matriz.colunas >= M_MAX) || (matriz.linhas < 0 || matriz.linhas >= N_MAX)){
        printf("ErrO: LIMITES DA MATRIX ESTOURADOS");
        exit(-1);
    }

    printf("%s (%dx%d)\n", matriz.name, matriz.linhas, matriz.colunas);

    for (int i=0; i<matriz.linhas; i++){
        printf("\t| ");
        for(int j=0; j<matriz.colunas; j++){
            printf("%f ",matriz.values[i][j]);
        }
        printf("|\n");
    }
    printf("\n");
}

Matrix matrix_sum(Matrix matriz_1,Matrix matriz_2){
    if(matriz_1.linhas==matriz_2.linhas && matriz_1.colunas==matriz_2.colunas){
        Matrix resultante=matrix_zeros("Soma",matriz_1.linhas,matriz_1.colunas);
        for (int i=0; i<resultante.linhas; i++){
            for(int j=0; j<resultante.colunas; j++){
                resultante.values[i][j]=matriz_1.values[i][j]+matriz_2.values[i][j];
            }   
        }
        return resultante;
    }
    printf("Não é posśivel somar essas duas matrizes\n");
    exit(-1);
}

Matrix matrix_subtraction(Matrix matriz_1,Matrix matriz_2){
    if(matriz_1.linhas==matriz_2.linhas && matriz_1.colunas==matriz_2.colunas){
        Matrix resultante=matrix_zeros("Subtração",matriz_1.linhas,matriz_1.colunas);
        for (int i=0; i<resultante.linhas; i++){
            for(int j=0; j<resultante.colunas; j++){
                    resultante.values[i][j]=matriz_1.values[i][j]-matriz_2.values[i][j];
            }
        }
        return resultante;
    }
    printf("Não é posśivel subtrair essas duas matrizes\n");
    exit(-1);
}

Matrix matrix_multiplication(Matrix matriz_1,Matrix matriz_2){

    // printf("%d linhas %d colunas\n",matriz_2.linhas, matriz_1.colunas );

    if(matriz_1.colunas==matriz_2.linhas){
        float valor=0;
        Matrix resultante=matrix_ones("Multiplicação",matriz_1.linhas,matriz_2.colunas);
        for (int i=0; i<resultante.linhas; i++){
            for(int j=0; j<resultante.colunas; j++){
                for(int k=0;k<matriz_1.colunas;k++){
                    valor+=matriz_1.values[i][k]*matriz_2.values[k][j];
                }
                resultante.values[i][j]=valor;
                valor=0;
            }
        }
        return resultante;
    }
    else{
        printf("Não é possível multiplicar essas duas matrizes\n");
        exit(-1);
    }
}

Matrix matrix_scalar(Matrix matriz,double escalar){
    for (int i=0; i<matriz.linhas; i++){
        for(int j=0; j<matriz.colunas; j++){
                matriz.values[i][j]*= escalar;
        }
    }
    return matriz;
}

Matrix matrix_transposed(Matrix matriz){
    Matrix aux=matriz;
    for (int i=0; i<matriz.linhas; i++){
        for(int j=0; j<matriz.colunas; j++){
                matriz.values[i][j]= aux.values[j][i];
        }
    }
    return matriz;
}

double matrix_det_ordem2(Matrix matriz){
    return matriz.values[0][0]*matriz.values[1][1]-matriz.values[0][1]*matriz.values[1][0];
}

Matrix matrix_1_ordem_menor(Matrix matriz, int linhas, int coluna){
    int linha_aux=0;
    int coluna_aux=0;
    Matrix matriz_ordem=matrix_zeros("ordem_menor",matriz.linhas-1,matriz.linhas-1);
    for(int i=0;i<matriz.linhas;i++){
        for(int j=0;j<matriz.linhas;j++){
            if(i!=linhas && j!=coluna){
                matriz_ordem.values[linha_aux][coluna_aux]=matriz.values[i][j];
                coluna_aux++;
                if(coluna_aux==matriz.linhas-1){
                    linha_aux++;
                    coluna_aux=0;
                }
            }
        }
    }
    return matriz_ordem;
}

double matrix_determinant(Matrix matriz){
    if(matriz.linhas==matriz.colunas){
        if(matriz.linhas==1){
            return matriz.values[0][0];
        }
        else if(matriz.linhas==2){
            return matrix_det_ordem2(matriz);
        }
        else{
            float aux=0;
            for(int i=0;i<matriz.linhas;i++){
                Matrix matriz_aux=matrix_1_ordem_menor(matriz,i,0);
                // matrix_print(matriz_aux);
                aux+=matriz.values[i][0]*pow(-1,i+2)*matrix_determinant(matriz_aux);
            }
            return aux;
        }
    }
    else{
        printf("Não é possível calcular o determinante\n");
        exit(-1);
    }
}

Matrix matrix_submatrix(Matrix matrix, int line, int column){
    Matrix submatrix = matrix_ones("Submatrix", matrix.linhas-1,matrix.colunas-1);

    for (int i=0; i<submatrix.linhas; i++){
        for(int j=0; j<submatrix.colunas; j++){
            if (i<line){
                if (j<column)
                    submatrix.values[i][j] = matrix.values[i][j];
                else
                    submatrix.values[i][j] = matrix.values[i][j+1];
            }
            else{
                if (j<column)
                    submatrix.values[i][j] = matrix.values[i+1][j];
                else
                    submatrix.values[i][j] = matrix.values[i+1][j+1];
            }
        }   
    }
    return submatrix;
}

Matrix matrix_cofactor(Matrix matrix){
    Matrix cofactor = matrix_ones("Inverse",matrix.linhas,matrix.colunas);
    int det = 0;

    for (int i=0; i<cofactor.linhas; i++){
        for(int j=0; j<cofactor.colunas; j++){
            Matrix aux = matrix_submatrix(matrix, i, j);
            det = matrix_determinant(aux);
            cofactor.values[i][j] = pow((-1),i+j+2)*det ;
        }   
    }
    return cofactor;
}

Matrix matrix_inverse(Matrix matrix){
    double det_a = matrix_determinant(matrix);


    if(matrix.linhas!=matrix.colunas)
        printf("Não é possível calcular a inversa de uma matriz não quadrada\n");
    if(det_a==0)
        printf("Não é possível calcular a inversa de uma matriz com det = 0\n");

    return matrix_scalar(matrix_transposed(matrix_cofactor(matrix)), 1/det_a);
}