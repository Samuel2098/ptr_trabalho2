#ifndef __STRUCTS_H
#define __STRUCTS_H
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "matrix.h"
#include "../monitor.h"

#define PI 3.14159265
#define TIME_SIMULATION 20
#define TAMANHO 167
#define R 0.3
#define a1 0.8
#define a2 0.8

typedef struct{
    double tempo;
    double tempoPassado;
    int posicao1;
    int posicao2;
    int posicao3;
    int posicao4;
    int posicao5;

}Arguments;


#endif