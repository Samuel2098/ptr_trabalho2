#include "estatisticas.h"

double med, var;
int posicaoglobal;

void estatisticas(double vector[], int posicao){
    posicaoglobal = posicao;
    printf("\n");
    printf("media: %.5lf\n", media(vector));
    printf("variancia: %.5lf\n", variancia(vector));
    printf("desvio_padrao: %.5lf\n", desvio_padrao(vector));
    printf("maximo: %.5lf\n", maximo(vector));
    printf("minimo: %.5lf\n", minimo(vector));
}

double media(double vector[]){
    double soma;

    for(int i=0;i<posicaoglobal;i++){
        if(&vector[i]!=NULL){
            soma+=vector[i];
        }
    }

    med = soma/posicaoglobal;

    return med;
}

double variancia(double vector[]){

    for(int i=0;i<posicaoglobal;i++){
        if(&vector[i]!=NULL){
            var += ((vector[i]-med)*(vector[i]-med));
        }
    }
    var = var/(posicaoglobal-1);

    return var;
} 

double desvio_padrao(double vector[]){
    return sqrt(var);
}
double maximo(double vector[]){
    double max = vector[0];

    for(int i=1;i<posicaoglobal;i++){
        if(&vector[i]!=NULL){
            if(vector[i]>max){
                max=vector[i];
            }
        }
    }

    return max;
    // return;
}
double minimo(double vector[]){
    double min = vector[0];

    for(int i=1;i<posicaoglobal;i++){
        if(&vector[i]!=NULL){
            if(vector[i]<min){
                min=vector[i];
            }
        }
    }

    return min;
}

void vetor(double vector[],int tamanho){
    printf("\n[");
    for(int i=0;i<tamanho;i++){
        printf("%lf ",vector[i]);
    }
    printf("]\n");
}