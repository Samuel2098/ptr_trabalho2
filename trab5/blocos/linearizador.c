#include "linearizador.h"

void linearizador_produz_u(Matrix *u,Matrix *x,Matrix *v){
    Matrix saida_linear;
    saida_linear = matrix_ones("SaidaLinear",2,2);

    //ja ta invertida
    saida_linear.values[0][0] = cos(x->values[2][0]);
    saida_linear.values[0][1] = sin(x->values[2][0]);
    saida_linear.values[1][0] = -sin(x->values[2][0])/R;
    saida_linear.values[1][1] = cos(x->values[2][0])/R;

    saida_linear = matrix_multiplication(saida_linear,*v);
    u->values[0][0]=saida_linear.values[0][0];
    u->values[1][0]=saida_linear.values[1][0];
}


void linearizador_consome_v(Matrix *v){
 
    return;
}
