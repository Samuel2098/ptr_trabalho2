#include "../biblioteca/structs.h"
#include "entrada.h"

float entrada_produz(Arguments *arg,Matrix *ref){
    ref->values[0][0]=(5/PI)*(cos(0.2*PI*arg->tempo));
    if(arg->tempo<10){
        ref->values[1][0]=(5/PI)*(sin(0.2*PI*arg->tempo));
    }
    else{
        ref->values[1][0]=-(5/PI)*(sin(0.2*PI*arg->tempo));
    }
}
