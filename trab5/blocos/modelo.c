#include "modelo.h"

void modelo_produz_ym(Matrix *ref, Matrix *ym, Matrix *ymd,Arguments args){
    double novo_ymxd,novo_ymyd;
    novo_ymxd=a1*(ref->values[0][0]-ym->values[0][0]);
    novo_ymyd=a2*(ref->values[1][0]-ym->values[1][0]);

    ym->values[0][0]=((novo_ymxd+ymd->values[0][0])*(args.tempo-args.tempoPassado)/2);
    ym->values[1][0]=((novo_ymyd+ymd->values[1][0])*(args.tempo-args.tempoPassado)/2);

    ymd->values[0][0]=novo_ymxd;
    ymd->values[1][0]=novo_ymyd;
}
