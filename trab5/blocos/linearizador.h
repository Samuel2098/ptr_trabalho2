#ifndef __LINEARIZADOR_H
#define __LINEARIZADOR_H
#include "../biblioteca/structs.h"

void linearizador_produz_u(Matrix *u, Matrix *x,Matrix *v);
// void linearizador_consome_x(Matrix *x);
void linearizador_consome_v(Matrix *v);
// void initLinearizador();

#endif