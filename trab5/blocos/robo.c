#include "robo.h"

Matrix robot_out;

void robo_produz_y(Matrix *y,Matrix *x){
    Matrix robot_calculated, robot_convert, robot_f;
    robot_convert = matrix_ones("RobotConvert",2,3);
    robot_f = matrix_ones("RobotF",2,1);
    robot_calculated=matrix_ones("RobotCalculated",2,1);
    
    robot_convert.values[0][0]=1;
    robot_convert.values[0][1]=0;
    robot_convert.values[0][2]=0;
    robot_convert.values[1][0]=0;
    robot_convert.values[1][1]=1;
    robot_convert.values[1][2]=0;

    robot_f.values[0][0]=R*cos(x->values[2][0]);
    robot_f.values[1][0]=R*sin(x->values[2][0]);

    robot_calculated=matrix_multiplication(robot_convert,*x);
    robot_calculated=matrix_sum(robot_calculated,robot_f);

    // matrix_print(robot_calculated);
    y->values[0][0]=robot_calculated.values[0][0];
    y->values[1][0]=robot_calculated.values[1][0];
}
void robo_produz_x(Matrix *x){
    printf("%lf , %lf , %lf\n",x->values[0][0],x->values[1][0],x->values[2][0]);
}
void robo_consome_u(Arguments arg, Matrix *u, Matrix *x, Matrix *xd){
    double x1Novo,x2Novo,x3Novo;
    x1Novo=cos(x->values[2][0])*(u->values[0][0]);
    x2Novo=sin(x->values[2][0])*(u->values[0][0]);
    x3Novo=u->values[1][0];

    x->values[0][0]=(x1Novo+xd->values[0][0])*(arg.tempo-arg.tempoPassado)/2;
    x->values[1][0]=(x2Novo+xd->values[1][0])*(arg.tempo-arg.tempoPassado)/2;
    x->values[2][0]=(x3Novo+xd->values[2][0])*(arg.tempo-arg.tempoPassado)/2;

    xd->values[0][0]=x1Novo;
    xd->values[1][0]=x2Novo;
    xd->values[2][0]=x3Novo;
}