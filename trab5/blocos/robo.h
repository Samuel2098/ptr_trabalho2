#ifndef __ROBO_H
#define __ROBO_H
#include "../biblioteca/structs.h"

void robo_produz_y(Matrix *y,Matrix *x);
void robo_produz_x(Matrix *x);
void robo_consome_u(Arguments arg, Matrix *u,Matrix *x, Matrix *xd);

#endif