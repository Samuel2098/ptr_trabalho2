#ifndef __SYSTEM_SIMULATION_H
#define __SYSTEM_SIMULATION_H

// #ifndef SYSTEM_OPERATION
// #define SYSTEM_OPERATION

#include "system_operations.c"
#include <stdio.h>
#include <stdlib.h>

// #endif

void create_U(float time,Matrix *U);
void print_Yf(Matrix *Y_out);

#endif