#include "system_simulation.h"


void create_U(float time,Matrix *U){
    Matrix auxiliar = robot_input(time);
    *U=auxiliar;
    printf("[%lf, %lf]\t",U->values[0][0],U->values[1][0]);
}

void print_Yf(Matrix *Y_out){
     printf("[%lf, %lf, %lf]\n",Y_out->values[0][0],Y_out->values[1][0],Y_out->values[2][0]);
}