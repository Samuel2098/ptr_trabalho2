#include "system_simulation.c"
#include "system_sampling.c"
#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <unistd.h>
#include <time.h>

#define TIME_SIMULATION 20

sem_t consome_1,produz_1,consome_2,produz_2;
sem_t trava_1,trava_2;
Matrix *U=(Matrix*)malloc(sizeof(Matrix));
Matrix *Yf= (Matrix*)malloc(sizeof(Matrix));

// typedef struct{
//     float time;
//     int D;
// }Arguments;

// Arguments Args;

double periodo_tarefa_1[TIME_SIMULATION];
double periodo_tarefa_2[TIME_SIMULATION];

double jitter_tarefa_1[TIME_SIMULATION];
double jitter_tarefa_2[TIME_SIMULATION];

void estatisticas(){
    double media1,variancia1,desvio_padrao1,maximo1,minimo1;
    double media2,variancia2,desvio_padrao2,maximo2,minimo2;
    double soma1,soma2;

    for(int i=0;i<TIME_SIMULATION;i++){
        soma1+=periodo_tarefa_1[i];
        soma2+=periodo_tarefa_2[i];
    }

    media1=soma1/TIME_SIMULATION;
    media2=soma2/TIME_SIMULATION;

    printf("\n");
    for(int i=0;i<TIME_SIMULATION;i++){
        variancia1 += ((periodo_tarefa_1[i]-media1)*(periodo_tarefa_1[i]-media1));
        variancia2 += ((periodo_tarefa_2[i]-media2)*(periodo_tarefa_2[i]-media2));

        // printf("%.10lf ",periodo_tarefa_1[i]);
    }
    variancia1/(TIME_SIMULATION-1);
    variancia2/(TIME_SIMULATION-1);

    desvio_padrao1=sqrt(variancia1);
    desvio_padrao2=sqrt(variancia2);

    maximo1=0;
    maximo2=0;
    minimo1=999999999;
    minimo2=999999999;

    printf("\n");
    for(int i=0;i<TIME_SIMULATION;i++){
        if(periodo_tarefa_1[i]>maximo1){
            maximo1=periodo_tarefa_1[i];
        }
        if(periodo_tarefa_2[i]>maximo2){
            maximo2=periodo_tarefa_2[i];
        }
        if(periodo_tarefa_1[i]<minimo1){
            minimo1=periodo_tarefa_1[i];
        }
        if(periodo_tarefa_2[i]<minimo2){
            minimo2=periodo_tarefa_2[i];
        }
        // printf("%.10lf ",periodo_tarefa_2[i]);
    }
    printf("\n");
    printf("media1: %.10lf\n", media1);
    printf("media2: %.10lf\n", media2);
    printf("variancia1: %.10lf\n", variancia1);
    printf("variancia2: %.10lf\n", variancia2);
    printf("desvio_padrao1: %.10lf\n", desvio_padrao1);
    printf("desvio_padrao2: %.10lf\n", desvio_padrao2);
    printf("maximo1: %.10lf\n", maximo1);
    printf("maximo2: %.10lf\n", maximo2);
    printf("minimo1: %.10lf\n", minimo1);
    printf("minimo2: %.10lf\n", minimo2);


}

void* simulator(void *arg){
    clock_t Ticks;

    while(Args.time<=20){
        Ticks = clock();
        sem_wait(&produz_1);
        sem_wait(&trava_1);
            printf("%.2lf\t",Args.time);
            create_U(Args.time,U);
        sem_post(&trava_1);
        sem_post(&consome_1);

        sem_wait(&consome_2);
            sem_wait(&trava_2);
                print_Yf(Yf);
                Args.time+=0.05;
            sem_post(&trava_2);
        sem_post(&produz_2);
        usleep(30000);
    }
    
}

void* sampler(void *arg){
    clock_t Ticks;
    while(Args.time<=20){
        
        Ticks = clock();
        sem_wait(&consome_1);
        sem_wait(&trava_1);
            consome_U(Args.time,U);
        sem_post(&trava_1);
        sem_post(&produz_1);

        sem_wait(&produz_2);
            sem_wait(&trava_2);
                produz_Yf(Args.time, Yf, Args.D);
            sem_post(&trava_2);
        sem_post(&consome_2);
        usleep(50000);
    }
}

int main(int argc,char *argv[]){
    if (argc != 2){
        printf("Você precisa passar o valor de D como parâmetro\n");
        return -1;
    }else{
        Args.D = atoi(argv[1]);
        Args.time = 0;
    }

    sem_init(&consome_1, 0, 0);
    sem_init(&consome_2, 0, 0);
    sem_init(&produz_1, 0, 1);
    sem_init(&produz_2, 0, 1);
    sem_init(&trava_1, 0, 1);
    sem_init(&trava_2, 0, 1);
    
    pthread_t tarefa_1,tarefa_2;
    
    
    pthread_create(&tarefa_1,NULL,simulator,NULL);
    pthread_create(&tarefa_2,NULL,sampler,NULL);
    pthread_join(tarefa_1,NULL);
    pthread_join(tarefa_2,NULL);

    estatisticas();

    return 0;
}