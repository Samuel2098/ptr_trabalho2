#ifndef __OPERATIONS_H
#define __OPERATIONS_H

#include "trab2/matrix.c"
#include <stdio.h>
#include "math.h"

#define PI 3.14159265

Matrix_str robot_system();
Matrix_str robot_front(float D);
Matrix robot_input(float t);
char *strcatb(char *dst, const char *src);
Matrix_str matrix_integral(Matrix_str matrix);
char *integral(char *funcao);
float calcula_string(char *funcao,float t);
Matrix Matrix_str_to_Matrix_int(Matrix_str matrix,float t);
Matrix Matrix_str_to_Matrix_int2(Matrix_str matrix,Matrix qualquer);
int verifica(char *string);

#endif