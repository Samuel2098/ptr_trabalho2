#include "system_operations.h"
#include <string.h>

Matrix_str robot_system(){
    Matrix_str robot_sys = matrix_str_create("Robot System", 3, 2);

    strcpy(robot_sys.values[0][0], "sin(t)");
    strcpy(robot_sys.values[1][0], "cos(t)");
    strcpy(robot_sys.values[2][0], "0");
    strcpy(robot_sys.values[0][1], "0");
    strcpy(robot_sys.values[1][1], "0");
    strcpy(robot_sys.values[2][1], "1");

    return robot_sys;
}


Matrix_str robot_front(int D){
    Matrix_str robot_f = matrix_str_create("Robot Front", 3, 3);
    char aux[MAX_NAME_SIZE];
    char aux2[MAX_NAME_SIZE];
    char aux_2[MAX_NAME_SIZE];
    sprintf(aux2, "%lf",D*0.5);
    strcpy(aux_2,"sin(t)");
    strcpy(aux,"cos(t)");
    strcatb(aux,"*");
    strcatb(aux_2,"*");
    strcatb(aux,aux2);
    strcatb(aux_2,aux2);
    strcpy(robot_f.values[0][0], aux);
    strcpy(robot_f.values[0][1], "0.0");
    strcpy(robot_f.values[0][2], "0.0");
    strcpy(robot_f.values[1][0], "0.0");
    strcpy(robot_f.values[1][1], aux_2);
    strcpy(robot_f.values[1][2], "0.0");
    strcpy(robot_f.values[2][0], "0.0");
    strcpy(robot_f.values[2][1], "0.0");
    strcpy(robot_f.values[2][2], "1.0");

    return robot_f;
}

Matrix_str robot_front2(int D){
    Matrix_str robot_f = matrix_str_create("Robot Front", 3, 3);
    char aux[MAX_NAME_SIZE];
    char aux2[MAX_NAME_SIZE];
    char aux_2[MAX_NAME_SIZE];
    sprintf(aux2, "%lf",D*0.5);
    strcpy(aux_2,"sin(t)");
    strcpy(aux,"cos(t)");
    strcatb(aux,"*");
    strcatb(aux_2,"*");
    strcatb(aux,aux2);
    strcatb(aux_2,aux2);
    strcpy(robot_f.values[0][0], aux);
    strcpy(robot_f.values[1][0], aux_2);

    return robot_f;
}

Matrix robot_input(float t){
    Matrix U=matrix_zeros("Robot Input",2,1);
    
    if(t<0){
        U.values[0][0]=0;
        U.values[1][0]=0;
    }
    else if(t<10){
        U.values[0][0]=1;
        U.values[1][0]=0.2*PI;
    }
    else{
        U.values[0][0]=1;
        U.values[1][0]=-0.2*PI;
    }

    return U;
}

char * strcatb( char * dst, const char * src ){
   size_t len = strlen(src);
   memmove( dst + len, dst, strlen(dst)+1);
   memcpy( dst, src, len );
   return dst;
}

char *integral(char *funcao){
    float auxiliar=(float) atoll(&funcao[2]);
    if(strlen(funcao)>1){
        if(strcmp(funcao,"cos(t)")==0){
            strcpy(funcao,"sin(t)");
        }
        else if(strcmp(funcao,"sin(t)")==0){
            strcpy(funcao,"-cos(t)");
        }
        else if(strcmp(funcao,"e(t)")==0){
            strcpy(funcao,"e(t)");
        }
        else if(auxiliar!=0){
            char aux[MAX_NAME_SIZE];
            char aux2[MAX_NAME_SIZE];
            char aux3[MAX_NAME_SIZE];
            auxiliar=auxiliar+1;
            sprintf(aux2, "%i",auxiliar);
            strcpy(aux,aux2);
            strcatb(aux,"^");
            strcatb(aux,"t");
            strcatb(aux,"*");
            sprintf(aux3, "%.2lf",(float)1/auxiliar);
            strcatb(aux,aux3);
            strcpy(funcao,aux);
        }
    }
    else{
        if(strcmp(funcao,"0")==0){
            strcpy(funcao,"0");
        }
        else if(strcmp(funcao,"t")==0){
            strcpy(funcao,"0.5*t^2");
        }
        else{
            char aux[MAX_NAME_SIZE];
            strcpy(aux,"*t");
            strcatb(aux,funcao);
            strcpy(funcao,aux);
        }
    }
    return funcao;
}

Matrix_str matrix_integral(Matrix_str matrix){
    for(int linhas=0;linhas<matrix.linhas;linhas++){
        for(int colunas=0;colunas<matrix.colunas;colunas++){
            strcpy(matrix.values[linhas][colunas],integral(matrix.values[linhas][colunas]));
        }
    }
    return matrix;
}
int verifica(char *string){
    int contador=0;
    while(contador<strlen(string)){
        if(string[contador]=='*'){
            return contador;
        }
        contador++;
    }
    return -1;
}
int pertence(char *texto,char caracter){
    int contador=0;
    while(contador<strlen(texto)){
        if(texto[contador]==caracter){
            return contador;
        }
        contador++;
    }
    return -1;
}

float calcula_string(char *funcao,float t){
    char aux[MAX_NAME_SIZE];
    strcpy(aux,funcao);
    char auxiliar[MAX_NAME_SIZE];
    
    if(t<10){
            if(strcmp(aux,"-cos(t)")==0){
                return (-cos(0.2*PI*t))/(0.2*PI);
            }
            else if(strcmp(aux,"sin(t)")==0){
                return sin(0.2*PI*t)/(0.2*PI);
            }
            else if(strcmp(aux,"e(t)")==0){
                return exp(0.2*PI*t)/(0.2*PI);
            }
            else if((float) atoll(aux)==0){
                return 0;
            }
            else{
                return (float) atoll(aux)*t;
            }
    }
    else{
            if(strcmp(aux,"-cos(t)")==0){
                return (-1*cos(-0.2*PI*t))/(-0.2*PI);
            }
            else if(strcmp(aux,"sin(t)")==0){
                return sin(-0.2*PI*t)/(-0.2*PI);
            }
            else if(strcmp(aux,"e(t)")==0){
                return exp(-0.2*PI*t)/(-0.2*PI);
            }
            else if((float) atoll(aux)==0){
                return 0;
            }
            else{
                return (float) atoll(aux)*t;
            }
    }

}

float calcula_string2(char *funcao,float valor){
    char aux[MAX_NAME_SIZE];
    strcpy(aux,funcao);
    if(pertence(aux,'c')>-1){
        
        return (float) atoll(aux)*cos(valor);
    }
    else if(pertence(aux,'s')>-1){
        return (float) atoll(aux)*sin(valor);
    }
    else{
        return (float) atoll(aux);
    }
       
}

Matrix Matrix_str_to_Matrix_int(Matrix_str matrix,float t){
    Matrix aux = matrix_zeros("Matriz convertida",matrix.linhas,matrix.colunas);
    for(int linhas=0;linhas<matrix.linhas;linhas++){
        for(int colunas=0;colunas<matrix.colunas;colunas++){
            aux.values[linhas][colunas]=calcula_string(matrix.values[linhas][colunas],t);
        }
    }
    return aux;
}

Matrix Matrix_str_to_Matrix_int2(Matrix_str matrix,Matrix qualquer){
    Matrix aux = matrix_zeros("Matriz convertida",matrix.linhas,matrix.colunas);
    for(int linhas=0;linhas<matrix.linhas;linhas++){
        for(int colunas=0;colunas<matrix.colunas;colunas++){
            aux.values[linhas][colunas]=calcula_string2(matrix.values[linhas][colunas],qualquer.values[0][2]);
        }
    }
    return aux;
}