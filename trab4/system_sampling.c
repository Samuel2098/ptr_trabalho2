#include "system_sampling.h"

Matrix robot_out;

void consome_U(float time, Matrix *U){
    Matrix_str robot_sys, robot_integrated; 
    Matrix ident, robot_calculated;

    robot_sys = robot_system();

    robot_integrated = matrix_integral(robot_sys);

    ident = matrix_identity("Matriz identidade",3,3);

    robot_calculated = Matrix_str_to_Matrix_int(robot_integrated,time);
    robot_out = matrix_multiplication(robot_calculated,*U);
}

void produz_Yf(float time, Matrix *Y, int D){
    Matrix_str robot_f;
    Matrix robot_calculated;
    
    robot_f=robot_front(D);
    robot_calculated=Matrix_str_to_Matrix_int2(robot_f,robot_out);
    robot_calculated=matrix_sum(robot_out,matrix_multiplication(robot_calculated,robot_out));
    
    *Y=robot_calculated;
    
   
}