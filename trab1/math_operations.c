#include<stdio.h>
#include "math_operations.h"

Operations math_operations_create(int num1,int num2){
    Operations dados;
    dados.num1=num1;
    dados.num2=num2;
    return dados;
}

int math_operations_sum(Operations dados){
    return dados.num1+dados.num2;
}

int math_operations_subtraction(Operations dados){
    return dados.num1-dados.num2;
}

void math_operations_print(Operations dados){
    printf("Soma: %d + %d = %d\n",dados.num1,dados.num2,math_operations_sum(dados));
    printf("Subtração: %d - %d = %d\n",dados.num1,dados.num2,math_operations_subtraction(dados));
}