#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "math_operations.c"
/*
    Dupla: Samuel Cristo da Fonseca - 21750601
           Thiago Costa - XXXXXXX
*/

//Identifica tipos dados
#define typeof(var) _Generic( (var),\
char: "Char",\
int: "Integer",\
float: "Float",\
char *: "String",\
void *: "Pointer",\
default: "Undefined")

int main(int argc,char *argv[]){
    printf("Membro1: Samuel Cristo da Fonseca\n");
    printf("Membro2: Thiago Costa\n");
    printf("-------------------------------------------\n");
    if(argc==3){
        int var1=atoi(argv[1]);
        int var2=atoi(argv[2]);
        char buf1[10],buf2[10];
        // inteiro para string. Até 10 algarísmos.
        sprintf(buf1, "%i", var1);
        sprintf(buf2, "%i", var2);
        if(!strcmp(buf1,argv[1]) && !strcmp(buf2,argv[2])){
            printf("Você colocou os parâmetros de forma correta\n");
            Operations dados=math_operations_create(atoi(argv[1]),atoi(argv[2]));
            math_operations_print(dados);
        }
        else{
            printf("Você colocou os parâmetros de forma errada\n");
        }
    }
    else{
        printf("Você colocou os parâmetros de forma errada\n");
    }

    printf("-------------------------------------------\n");
    printf("Fim\n");
    return 0;
}