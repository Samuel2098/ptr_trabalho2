#include"matrix.h"

int main(){
    printf("Laboratório 02\n");
    printf("Aluno1: Thiago Costa Antunes Afonso\n");
    printf("Aluno2: Samuel Cristo da Fonseca\n");
    printf("--------------------------------------------\n");

    Matrix m0, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10;
    
    m0 = matrix_zeros("Matrix Zeros", 4, 5);
    matrix_print(m0);

    m1 = matrix_ones("Matrix Ones", 4, 5);
    matrix_print(m1);

    m2 = matrix_identity("Matrix Identity", 2, 2);
    matrix_print(m2);

    m3 = matrix_zeros("Matrix 1", 2, 2);
    m3.values[0][0]=2;
    m3.values[1][0]=1;
    m3.values[0][1]=4;
    m3.values[1][1]=3;
    matrix_print(m3);

    m4 = matrix_zeros("Matrix 2", 3, 3);
    m4.values[0][0]=2;
    m4.values[1][0]=5;
    m4.values[2][0]=8;
    m4.values[0][1]=7;
    m4.values[1][1]=8;
    m4.values[2][1]=9;
    m4.values[0][2]=2;
    m4.values[1][2]=1;
    m4.values[2][2]=9;
    matrix_print(m4);

    m5 =  matrix_sum(m2, m3);
    matrix_print(m5);

    m6 =  matrix_subtraction(m2, m3);
    matrix_print(m6);

    m7 =  matrix_multiplication(m2, m3);
    matrix_print(m7);

    m8 = matrix_scalar(m3, 4);
    matrix_print(m8);

    m9 = matrix_transposed(m4);
    matrix_print(m9);

    int det = matrix_determinant(m3);
    printf("determinante da %s: %d\n\n",m4.name, det);

    det = matrix_determinant(m4);
    printf("determinante da %s: %d\n\n",m4.name, det);

    
    m10 = matrix_inverse(m3);
    matrix_print(m10);

    matrix_print(m4);
    matrix_print(matrix_inverse(m4));
    matrix_print(matrix_multiplication(m4,matrix_inverse(m4)));
    
    printf("--------------------------------------------\n");
    printf("Tchau\n");
}