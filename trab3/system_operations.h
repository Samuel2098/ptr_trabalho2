#ifndef __OPERATIONS_H
#define __SYSTEM_SIMULATION_H

#include "trab2/matrix.h"
#include "math.h"

#define PI 3.14159265

Matrix_str robot_system();
Matrix robot_input(int t);
char *strcatb(char *dst, const char *src);
Matrix_str matrix_integral(Matrix_str matrix);
char *integral(char *funcao);
double calcula_string(char *funcao,int t);
Matrix Matrix_str_to_Matrix_int(Matrix_str matrix,int t);
#endif