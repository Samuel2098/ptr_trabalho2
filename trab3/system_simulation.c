#include "system_simulation.h"
#include "system_operations.c"

void robot_simulation(){
    Matrix_str robot_sys, robot_integrated; 
    Matrix robot_in, robot_out, ident, robot_calculated;

    robot_sys = robot_system();

    robot_integrated = matrix_integral(robot_sys);

    ident = matrix_identity("Matriz identidade",3,3);

    for(int t=0;t<20;t++){
        printf("%d\t",t);

        robot_in = robot_input(t);
        printf("[%lf, %lf]",robot_in.values[0][0],robot_in.values[1][0]);
        
        robot_calculated = Matrix_str_to_Matrix_int(robot_integrated,t);
        robot_out = matrix_multiplication(robot_calculated,robot_in);
        printf("\t[%lf, %lf, %lf]\n",robot_out.values[0][0],robot_out.values[1][0], robot_out.values[2][0]);

    }
}
